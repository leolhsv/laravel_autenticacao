<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\User;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function loginSocial(Request $request)
    {

        $this->validate($request, [
           'social_type' => 'required|in:google,github'
        ]);

        $socialType = $request->get('social_type');
        \Session::put('social_type', $socialType);

        return \Socialite::driver($socialType)->redirect();
    }

    public function loginCallback()
    {

        $socialType = \Session::pull('social_type'); //o método pull pega o valor e depois destrói da sessão
        $userSocial = \Socialite::driver($socialType)->user();

        $user = User::where('email', $userSocial->email)->first();

        if (!$user) {

            if ($socialType == 'github') {
                $name = $userSocial->nickname;
            } elseif ($socialType == 'google') {
                $name = $userSocial->name;
            }

            $user = User::create([
                'name'     => $name,
//                'name' => 'okokok',
                'email'    => $userSocial->email,
//                'password' => bcrypt(str_random(8));
                'password' => bcrypt('123456'),
                'role'     => User::ROLE_USER,
                'phone'    => '000',
                'cpf'      => '000'
            ]);

        }
        \Auth::login($user);
        return redirect()->intended($this->redirectPath());
    }

    public function redirectTo()
    {
        return \Auth::user()->role == User::ROLE_ADMIN ? '/admin/home' : '/home';
    }

    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        return $this->loggedOut($request) ?: redirect($request->is('admin/*') ? '/admin/login' : '/login');

    }

    /**
     * Método para fazer login com outros campos
     * @param Request $request
     * @return array
     */
    protected function credentials(Request $request)
    {
        $data = $request->only($this->username(),'password');
        $usernameKey = $this->usernameKey();

        if ($usernameKey != $this->username()) {

            $data[$this->usernameKey()] = $data[$this->username()];
            unset($data[$this->username()]);

        }

        return $data;
    }


    /**
     * Método para fazer login com outros campos
     * @return string
     */
    protected function usernameKey()
    {
        $email = \Request::get('email');

        $validator = \Validator::make([
           'email' => $email,
        ],['email' => 'cpf']);

        if (!$validator->fails()) {
            return 'cpf';
        }
        if (is_numeric($email)) {
            return 'phone';
        }
        return 'email';
    }

}
