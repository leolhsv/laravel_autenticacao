<?php
/**
 * Created by PhpStorm.
 * User: Leo
 * Date: 16/08/2019
 * Time: 12:10
 */

function layout()
{
    return \Request::is('admin/*') ? 'layouts.admin' : 'layouts.app';
}

function isAdmin()
{
    return \Request::is('admin/*') ? true : false;
}