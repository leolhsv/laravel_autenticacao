<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::post('/login/social', 'Auth\LoginController@loginSocial');
Route::get('/login/callback', 'Auth\LoginController@loginCallback');



Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'admin', 'as' => 'admin.'], function() {

    Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
    Route::post('login', 'Auth\LoginController@login');
    Route::post('logout', 'Auth\LoginController@logout')->name('logout');

    Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');


    Route::group(['middleware' => 'can:admin'], function() {
        Route::get('/home', 'HomeController@index')->name('home');
    });

    Route::post('/login/social', 'Auth/LoginController@loginSocial');
    Route::get('/login/callback', 'Auth/LoginController@loginCallback');

});

//Route::group(['middleware' => 'auth'], function() {
//    Route::get('/teste', function() {
//        echo "teste";
//    });
//});

Route::group(['middleware' => 'guest'], function() {
    Route::get('/teste', function() {
        echo "teste";
    });
});


Route::get('/auth', function(\Illuminate\Http\Request $request){
//    dd($request->user());
//    dd(\Auth::user());
//    dd(\Auth::check()); //Verifica se está autenticado
//    dd(\Auth::id());
    
});







//http - não mantém estado -

//sessão - informação guardada no servidor - guarda os dados do usuários (nome, email, etc)
//cookie da sessão

//o cookie corresponde a uma sessão guardada no servidor

//cookies - informação guardada no browser do client

